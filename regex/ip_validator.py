import re
###################################################
# IP address validator 
# details: https://en.wikipedia.org/wiki/IP_address
# Student should enter function on the next lines.
# 
# 
# First function is_valid_ip() should return:
# True if the string inserted is a valid IP address
# or False if the string is not a real IP
# 
# 
# Second function get_ip_class() should return a string:
# "X is a class Y IP" or "X is classless IP" (without the ")
# where X represent the IP string, 
# and Y represent the class type: A, B, C, D, E
# ref: http://www.cloudtacker.com/Styles/IP_Address_Classes_and_Representation.png
# Note: if an IP address is not valid, the function should return
# "X is not a valid IP address"
###################################################


def is_valid_ip(ip):
    """
    Checks if an IP is a valid IPv4 address.

    The address may be a host, broadcast or network address,
    as long as it is valid the function will return True.
    :param ip: IP address to be checked
    :return: True if IP address is valid, False otherwise
    """
    # verify if the string has the right format
    match = re.findall(r'^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$', ip)
    if not match or len(match) > 1:
        # wrong format
        return False

    # cast strings to integers
    num_list = list(map(int, match[0]))

    # check if all numbers smaller than 256
    return all(num < 256 for num in num_list)


def get_ip_class(ip):
    """
    Indicates to what IPv4 class the IP address belongs
    :param ip: IPv4 address
    :return: string indicating what class the IP belongs to
    """
    # check if the IP is valid
    if not is_valid_ip(ip):
        return "{} is not a valid IP address".format(ip)
    # we only care about the first byte
    match = re.findall(r'^\d+', ip)
    first_byte = int(match[0])

    # special IP addresses (local identification and loopback)
    if first_byte == 0 or first_byte == 127:
        return "{} is a classless IP".format(ip)

    # upper limits of the classes and list of different classes
    limits = [128, 192, 224, 240, 256]
    classes = ["A", "B", "C", "D", "E"]
    # map classes to their upper limit
    ip_class = next(x[0] for x in zip(classes, limits) if x[1] > first_byte)

    return "{} is a class {} IP".format(ip, ip_class)
