import re
###################################################
# Parse internal CRC counters of a Nexus
# The students will have to parse the next output, and return a dictionary,
# with the parsed info
# 
# How the text input will look like:
#
# --------------------------------------------------------------------------------
# Port          Align-Err    FCS-Err   Xmit-Err    Rcv-Err  UnderSize OutDiscards
# --------------------------------------------------------------------------------
# Eth1/1                0          0          0          0          0           0
# Eth1/2                0          0          0          0          0           0
# Eth1/3                0          0          0          0          0           0
# Eth1/4                0          0          0          0          0           0
# <snip>
#
# --------------------------------------------------------------------------------
# Port         Single-Col  Multi-Col   Late-Col  Exces-Col  Carri-Sen       Runts
# --------------------------------------------------------------------------------
# Eth1/1                0          0          0          0          0           0
# Eth1/2                0          0          0          0          0           0
# <snip>
#
# --------------------------------------------------------------------------------
# Port          Giants SQETest-Err Deferred-Tx IntMacTx-Er IntMacRx-Er Symbol-Err
# --------------------------------------------------------------------------------
# Eth1/1             0          --           0           0           0          0
# Eth1/2             0          --           0           0           0          0
#
# How the output must look like:
#
# int_dict = {
# 	"Eth1/1": {
# 		"Align-Err": 0,
# 		"FCS-Err": 0,
# 		...
# 	}
# }
###################################################


def get_error_counters(text):
    """
    Parses the text and stores error counter values in a dictionary.
    :param text: string containing tables with error counters
    :return: dict mapping interfaces to their counter values
    """
    interfaces_dict = {}
    # fetch the lines containing the error descriptions
    err_lines = re.findall(r'Port(.+)', text)
    # fetch the lines containing interface values
    int_lines = re.findall(r'(Eth\d+\/\d+)(.*)', text)

    # save all the interfaces already visited
    int_visited = set()
    error_iter = iter(err_lines)
    errors = re.findall(r'[\w-]+', next(error_iter))

    for (intf, line) in int_lines:
        # if interface never visited, create new dictionary for it
        if intf not in interfaces_dict.keys():
            interfaces_dict[intf] = {}
        if intf in int_visited:
            # the values are part of a new table
            int_visited = set()
            # fetch the corresponding error descriptions
            errors = re.findall(r'[\w-]+', next(error_iter))

        # save values in dict
        values = re.findall(r'[\d-]+', line)
        for (error, val) in zip(errors, values):
            # try to cast to integer
            try:
                val = int(val)
            except ValueError:
                pass
            interfaces_dict[intf][error] = val
        int_visited.add(intf)

    return interfaces_dict
