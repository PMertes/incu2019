import re
###################################################
# Duplicate words 
# The students will have to find the duplicate words in the given text,
# and return the string without duplicates.
#
###################################################


def remove_duplicates(text):
    """
    Removes duplicate consecutive words.
    :param text: string containing duplicate words
    :return: string without duplicates
    """
    return re.sub(r"\b(\w+)\b(?:\s+\b\1\b)+", r"\1", text)
