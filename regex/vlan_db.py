import re
from pprint import pprint
###################################################
# Parse vlan status
# 
# Students will need to parse the output of a "show vlan" from a NXOS devices
# and return a dictionary with the parsed info
# 
# How the text input will look like:
# 
# 
# VLAN Name                             Status    Ports
# ---- -------------------------------- --------- -------------------------------
# 1    default                          active    Po213, Eth1/1, Eth1/2, Eth1/3
#                                                 Eth1/4
# 10   Vlan10                           active    Po1, Po10, Po111, Po213, Eth1/2
#                                                 Eth1/5, Eth1/16, Eth1/17
#                                                 Eth1/18, Eth1/49, Eth1/50
# How the output must look like:
# 
# vlan_db = {
# 	"1": {
# 		"Name": "default",
# 		"Status": "active",,
# 		"Ports": ["Po213", "Eth1/1"...]
# 	},
# 	"2":{},
# 	...
# }
#
###################################################


def get_vlan_db(text):
    """
    Parse the info contained in the output of the "show vlan" command.
    :param text: command output string
    :return: dict containing information parsed from command
    """
    vlan_db = {}
    # get all the lines containing values
    values = re.findall(r"(\d+)\s+(\w+)\s+(\w+)\s+((?:,?[\s\n]+(?:Po\d+|Eth\d+\/\d+))+)", text)

    # store those values in the dict
    for val in values:
        vlan, name, status, ports = val
        vlan_db[vlan] = {}
        vlan_db[vlan]["Name"] = name
        vlan_db[vlan]["Status"] = status
        # the string containing the ports has to be converted to a list
        ports_list = re.findall(r"(Po\d+|Eth\d+\/\d+)", ports)
        vlan_db[vlan]["Ports"] = ports_list
    return vlan_db
