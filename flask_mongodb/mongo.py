import pymongo
import sys
from pprint import pprint


def connect():
    """
    Connect to the "address_book" collection of the Incubator MongoDB database.
    :return: Collection object of the "address_book" collection
    """
    mc = pymongo.MongoClient("127.0.0.1", 27017)
    return mc.incubator["address_book"]


def check_document(doc):
    """
    Checks if a dictionary representing a document has all the necessary fields.
    :param doc: dictionary representing a document
    :return: Boolean indicating if yes or not the document contains all the fields
    """
    # Check if all necessary fields are in the document
    if "firstname" not in doc.keys():
        print("Each entry must contain a first name.\nKey: firstname", file=sys.stderr)
        return False
    if "lastname" not in doc.keys():
        print("Each entry must contain a last name.\nKey: lastname", file=sys.stderr)
        return False
    if "userid" not in doc.keys():
        print("Each entry must contain a user ID\nKey: userid", file=sys.stderr)
    if not any(key in doc.keys() for key in ["homephone", "workphone"]):
        print("Each entry must contain a home and/or work phone number.\nRespective keys: homephone, workphone",
              file=sys.stderr)
        return False

    # Check if no additional unknown key is in the entry
    return check_only_known_keys(doc)


def check_only_known_keys(doc):
    """
    Checks if an entry contains only known keys and not some undesired new ones
    :param doc: document representing a new or updated entry for the DB
    :return: Boolean, indicating if yes or no
    """
    if not all(key in ["userid", "firstname", "lastname", "email", "homephone", "workphone"] for key in doc.keys()):
        print("You can only use the following keys: userid, firstname, lastname, email, homephone, workphone",
              file=sys.stderr)
        return False
    return True


def insert_new_entry(doc):
    """
    Stores a dictionary into the DB
    :param doc: dictionary of the document to be stored
    :return: Boolean indicating if the insert was successful
    """
    # Check if necessary fields contained in doc
    if not check_document(doc):
        return False

    # Check if a user with such an ID is already contained in the DB
    if get_entries(doc["userid"]):
        return False

    # If all necessary fields in document, insert it.
    col = connect()
    return col.insert_one(doc).acknowledged


def update_entry(new_values, userid=None, firstname=None, lastname=None):
    """
    Updates an entry matching a first name and last name.
    :param new_values: new values for the entry
    :param userid: user ID of the DB entry
    :param firstname: first name of the DB entry
    :param lastname: last name of the DB entry
    :return: Boolean indicating if an entry got updated or not
    """
    # store the different arguments except new_values
    arguments = locals()
    arguments.pop("new_values")
    # Check if the new entry doesn't contain an undesired field
    if not check_only_known_keys(new_values):
        return False

    # Connect and replace entry
    col = connect()

    # add all arguments to the query whose value is not None
    query = form_query(arguments)

    return col.update_one(query, {"$set": new_values}).acknowledged


def display_entries(firstname=None, lastname=None):
    """
    Display entries matching a first and or last name.
    If no parameter is provided, all entries will be displayed.
    The matching documents are sorted by their last name.
    :param firstname: First name to filter on
    :param lastname: Last name to filter on
    :return: None
    """
    col = connect()

    # craft query
    query = {}
    if firstname:
        query["firstname"] = {"$regex": firstname}
    if lastname:
        query["lastname"] = {"$regex": lastname}

    # Find matches from DB
    matches = col.find(query, sort=[("lastname", pymongo.ASCENDING)])

    # Print matches
    for entry in matches:
        pprint(entry)


def get_entries(user_id=None):
    """
    Get one entry specified by ID or all.
    :param user_id: id of the user (different than the entry ID automatically assigned)
    :return: dictionary of the entry
    """
    col = connect()
    if not user_id:
        return col.find()
    query = {"userid": user_id}
    return col.find_one(query)


def delete_entries(userid=None, firstname=None, lastname=None):
    """
    Deletes entries matching user ID, the first or last name.
    :param userid: user ID to match
    :param firstname: first name to match
    :param lastname: last name to match
    :return: number of deleted entries
    """
    arguments = locals()
    col = connect()
    # craft query
    query = form_query(arguments)
    # delete entries
    return col.delete_many(query).deleted_count


def form_query(arguments):
    """
    Forms a query for the MongoDB by strapping all the keys whose value is None from the dictionary
    :param arguments: dictionary containing all the arguments given to a function
    :return: dictionary representing a valid query to the MongoDB
    """
    query = {}
    for arg in arguments.keys():
        if arguments[arg]:
            query[arg] = arguments[arg]
    return query
