import mongo
from bson.json_util import dumps
from flask import Flask, request

app = Flask(__name__)


@app.route("/getElems")
def list_elements():
    """
    List all elements in the MongoDB.
    :return: JSON object of all the elements
    """
    return dumps(mongo.get_entries())


@app.route("/elem/<user_id>", methods=["GET", "PUT", "DELETE"])
def handle_single_element(user_id):
    """
    Lists a single element in the MongoDB.
    :param user_id: The user ID of the element
    :return: JSON object
    """
    if request.method == "GET":
        return dumps(mongo.get_entries(user_id))
    elif request.method == "PUT":
        if request.json:
            mongo.update_entry(request.json, userid=user_id)
            return dumps(mongo.get_entries(user_id))
        return "You must include the new entry in a JSON inside the body.", 400
    else:
        if mongo.delete_entries(userid=user_id) > 0:
            return "The entry got successfully deleted.", 200
        return "Unable to delete the entry.\nVerify if there exists an entry with such a user ID.", 404


@app.route("/submit", methods=["POST"])
def submit_new_element():
    """
    Submits a new element to the MongoDB.
    :return: HTTP reply message and status code
    """
    if request.json:
        if mongo.insert_new_entry(request.json):
            return "The new element was submitted successfully.", 200
        return "Impossible to add this new entry.\n" \
               "Make sure you specified the right keys and that no other user with the same ID already exists.", 400
    return "You must submit the entry in the request body.", 400


app.run(host="0.0.0.0", port="7676")
