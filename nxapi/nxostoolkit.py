import json
import sys

import argparse
import requests


class Nexus:

    version = None
    platform = None

    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        # cookies and credentials
        self._cookie = None
        self._user = None
        self._password = None

    def authenticate(self, user: str, password: str):
        """
        Authenticate to the switch.
        :param user: username
        :param password: password corresponding to the username
        :return: None
        """
        # request parameters
        uri = "http://{}/api/mo/aaaLogin.json".format(self.ip)
        json_headers = {'Content-Type': 'application/json'}
        payload = {
                  "aaaUser": {
                    "attributes": {
                      "name": user,
                      "pwd": password
                    }
                  }
                }
        # HTTP POST request
        response = requests.post(uri,
                                 data=json.dumps(payload),
                                 headers=json_headers)
        resp = response.json()

        # try to fetch the cookie and then set the instance variables used to store credentials
        try:
            self._cookie = {"APIC-cookie": resp["imdata"][0]["aaaLogin"]["attributes"]["token"]}
            self._user = user
            self._password = password
        except KeyError:
            print("Unable to authenticate. Couldn't fetch a cookie from the HTTP response.")
            return None

    def get_interface_status(self, if_name: str):
        """
        Get the status of a specified interface.
        :param if_name: interface name
        :return: string representing the state: "up", "down", "unknown"
        """
        resp = self.post_show_command("show interface {}".format(if_name))

        # try to fetch corresponding element in dictionary
        try:
            state = resp["TABLE_interface"]["ROW_interface"]["state"]
        except KeyError:
            return "unknown"

        # if state is neither up or down, return "unknown"
        if not (state == "up" or state == "down"):
            return "unknown"

        return state

    def configure_interface_desc(self, if_name: str, description: str):
        """
        Modify the description of an interface.
        :param if_name: interface name
        :param description: new interface description
        :return: None
        """
        # request parameters
        uri = "http://{}:{}/api/mo/sys/intf/phys-[{}].json".format(self.ip, self.port, if_name)
        json_headers = {'Content-Type': 'application/json'}
        payload = {
                    "l1PhysIf": {
                        "attributes": {
                            "descr": description
                        }
                    }
                }
        resp = requests.post(uri, data=json.dumps(payload), headers=json_headers, cookies=self._cookie)

        # check if request was successful
        if resp.status_code != 200:
            print("Couldn't change description of {}.\nRequest reply status: {}".format(if_name, resp.status_code),
                  file=sys.stderr)

    def post_show_command(self, cmd: str):
        """
        Sends a HTTP POST request to the Nexus 9000 device containing a CLI show command.
        This method works with the NX-API JSON-RPC.
        :param cmd: CLI command to be executed on the Nexus device
        :return: dictionary containing result->body field of the json response. None, if not a successful status code.
        """
        # prepare request params
        uri = "http://{}:{}/ins".format(self.ip, self.port)
        jsonrpc_headers = {"Content-Type": "application/json-rpc"}
        payload = [
            {
                "jsonrpc": "2.0",
                "method": "cli",
                "params": {
                    "cmd": cmd,
                    "version": 1
                },
                "id": 1
            }
        ]

        # HTTP POST request
        response = requests.post(uri,
                                 data=json.dumps(payload),
                                 headers=jsonrpc_headers,
                                 auth=(self._user, self._password))

        if response.status_code != 200:
            # Not a successful request
            print('Unable to apply command "{}".\nRequest reply status: {}.'.format(cmd, response.status_code),
                  file=sys.stderr)
            return None
        # return only body as the rest of the response is not interesting
        return response.json()["result"]["body"]

    def set_version(self):
        """
        Set the version attribute through an API request
        :return: boolean indicating if setting was successful
        """
        resp = self.post_show_command("show version")
        # try to fetch the corresponding element from the dictionary
        try:
            Nexus.version = resp["kickstart_ver_str"]
            return True
        except KeyError:
            return False

    def set_platform(self):
        """
        Set the platform attribute through an API request.
        :return: boolean indicating if setting was successful
        """
        resp = self.post_show_command("show hardware")
        # try to fetch the corresponding element from the dictionary
        try:
            Nexus.platform = resp["TABLE_slot"]["ROW_slot"]["TABLE_slot_info"]["ROW_slot_info"][0]["model_num"]
            return True
        except KeyError:
            return False


# script testing the different methods
if __name__ == "__main__":
    # parse arguments
    parser = argparse.ArgumentParser(description="NX-OS toolkit")
    parser.add_argument("ip")
    parser.add_argument("port")
    parser.add_argument("user")
    parser.add_argument("password")
    args = parser.parse_args()

    # instantiate object
    nexus = Nexus(args.ip, args.port)
    # authenticate
    nexus.authenticate(args.user, args.password)
    if nexus.set_version():
        print("Nexus version: ", Nexus.version)
    if nexus.set_platform():
        print("Nexus platform: ", Nexus.platform)

    # get interface status
    intf = input("From which interface do you want to retrieve its status? ")
    print("{} interface status: {}".format(intf, nexus.get_interface_status(intf)))

    # configure interface description
    intf = input("From which interface do you want to configure the description? ")
    descr = input("What should be its description? ")
    nexus.configure_interface_desc(intf, descr)
